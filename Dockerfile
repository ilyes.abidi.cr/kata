# Use the official OpenJDK image as the base image
FROM openjdk:21-jdk-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the Maven executable and set execution permissions
COPY mvnw .
RUN chmod +x ./mvnw
COPY .mvn .mvn

# Copy the pom.xml and source code
COPY pom.xml .
COPY src src

# Build the application
RUN ./mvnw package -DskipTests

# Expose the port the app runs on
EXPOSE 8080

# Run the jar file
ENTRYPOINT ["java","-jar","/app/target/driveanddeliver-0.0.1-SNAPSHOT.jar"]
