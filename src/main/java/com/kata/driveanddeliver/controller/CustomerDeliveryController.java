package com.kata.driveanddeliver.controller;

import com.kata.driveanddeliver.model.CustomerDelivery;
import com.kata.driveanddeliver.service.CustomerDeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/book-delivery")
public class CustomerDeliveryController {

    @Autowired
    private CustomerDeliveryService customerDeliveryService;

    @PostMapping
    public ResponseEntity<EntityModel<CustomerDelivery>> bookDelivery(@RequestBody CustomerDelivery customerDelivery) {
        CustomerDelivery bookedDelivery = customerDeliveryService.bookTimeSlot(customerDelivery);
        return ResponseEntity.ok(toModel(bookedDelivery));
    }

    private EntityModel<CustomerDelivery> toModel(CustomerDelivery delivery) {
        EntityModel<CustomerDelivery> model = EntityModel.of(delivery);
        Link selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(CustomerDeliveryController.class)
                        .bookDelivery(delivery))
                .withSelfRel();
        model.add(selfLink);
        return model;
    }
}
