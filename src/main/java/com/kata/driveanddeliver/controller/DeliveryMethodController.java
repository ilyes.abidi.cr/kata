package com.kata.driveanddeliver.controller;

import com.kata.driveanddeliver.model.DeliveryMethod;
import com.kata.driveanddeliver.service.DeliveryMethodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@CrossOrigin
@RestController
@RequestMapping("/api/delivery-methods")
public class DeliveryMethodController {

    @Autowired
    private DeliveryMethodService deliveryMethodService;

    @GetMapping
    public List<EntityModel<DeliveryMethod>> getAllDeliveryMethods() {
        List<DeliveryMethod> deliveryMethods = deliveryMethodService.getAllDeliveryMethods();
        return deliveryMethods.stream()
                .map(this::toModel)
                .toList();
    }

    @GetMapping("/{id}")
    public EntityModel<DeliveryMethod> getDeliveryMethodById(@PathVariable Long id) {
        DeliveryMethod deliveryMethod = deliveryMethodService.getDeliveryMethodById(id);
        return toModel(deliveryMethod);
    }

    private EntityModel<DeliveryMethod> toModel(DeliveryMethod deliveryMethod) {
        EntityModel<DeliveryMethod> model = EntityModel.of(deliveryMethod);
        model.add(linkTo(WebMvcLinkBuilder.methodOn(DeliveryMethodController.class)
                .getDeliveryMethodById(deliveryMethod.getId()))
                .withSelfRel());
        return model;
    }
}
