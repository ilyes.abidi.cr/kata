package com.kata.driveanddeliver.controller;

import com.kata.driveanddeliver.model.DeliveryMethod;
import com.kata.driveanddeliver.model.TimeSlot;
import com.kata.driveanddeliver.service.DeliveryMethodService;
import com.kata.driveanddeliver.service.TimeSlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * made this public to just test the public security filter
 */

@RestController
@RequestMapping("/public/time-slots")
public class TimeSlotController {

    @Autowired
    private TimeSlotService timeSlotService;

    @Autowired
    private DeliveryMethodService deliveryMethodService;

    @GetMapping
    public ResponseEntity<List<EntityModel<TimeSlot>>> getAllTimeSlots() {
        // Cette méthode retourne tous les créneaux horaires, indépendamment de la méthode de livraison
        List<TimeSlot> slots = timeSlotService.getAllTimeSlots();
        List<EntityModel<TimeSlot>> resourceList = slots.stream()
                .map(this::toModel)
                .toList();
        return ResponseEntity.ok(resourceList);
    }

    @GetMapping("/available")
    public ResponseEntity<List<EntityModel<TimeSlot>>> getAllAvailableTimeSlots() {
        // Cette méthode retourne tous les créneaux horaires, indépendamment de la méthode de livraison
        List<TimeSlot> slots = timeSlotService.getAllAvailableTimeSlots();
        List<EntityModel<TimeSlot>> resourceList = slots.stream()
                .map(this::toModel)
                .toList();
        return ResponseEntity.ok(resourceList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EntityModel<TimeSlot>> getTimeSlotById(@PathVariable Long id) {
        TimeSlot slot = timeSlotService.getTimeSlotById(id);
        return ResponseEntity.ok(toModel(slot));
    }

    @GetMapping("/method/{methodId}")
    public ResponseEntity<List<EntityModel<TimeSlot>>> getTimeSlotsByMethod(@PathVariable Long methodId) {
        // Récupère la méthode de livraison spécifique
        DeliveryMethod method = deliveryMethodService.getDeliveryMethodById(methodId);

        if (method == null) {
            return ResponseEntity.notFound().build();
        }

        // Récupère les créneaux horaires associés à cette méthode de livraison
        List<TimeSlot> slots = timeSlotService.getTimeSlotsByMethod(method);

        List<EntityModel<TimeSlot>> resourceList = slots.stream()
                .map(this::toModel)
                .toList();
        return ResponseEntity.ok(resourceList);
    }

    private EntityModel<TimeSlot> toModel(TimeSlot slot) {
        EntityModel<TimeSlot> model = EntityModel.of(slot);
        Link selfLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(TimeSlotController.class)
                        .getTimeSlotById(slot.getId()))
                .withSelfRel();
        model.add(selfLink);
        return model;
    }
}
