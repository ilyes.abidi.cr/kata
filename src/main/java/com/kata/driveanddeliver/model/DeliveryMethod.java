package com.kata.driveanddeliver.model;


import jakarta.persistence.*;
import lombok.Data;


@Data
@Entity
@Table(name = "delivery_methods")
public class DeliveryMethod {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
}
