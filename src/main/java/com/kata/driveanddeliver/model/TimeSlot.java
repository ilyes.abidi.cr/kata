package com.kata.driveanddeliver.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
@Table(name = "time_slots")
public class TimeSlot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "delivery_method_id")
    private DeliveryMethod deliveryMethod;

    // Escaping the reserved keyword
    @Column(name = "\"day\"")
    private LocalDate day;

    private LocalTime startTime;

    private LocalTime endTime;

    private Boolean isBooked;
}
