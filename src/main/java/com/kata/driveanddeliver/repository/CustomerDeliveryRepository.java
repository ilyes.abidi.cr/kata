package com.kata.driveanddeliver.repository;

import com.kata.driveanddeliver.model.CustomerDelivery;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerDeliveryRepository extends JpaRepository<CustomerDelivery, Long> {
}
