package com.kata.driveanddeliver.repository;

import com.kata.driveanddeliver.model.DeliveryMethod;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeliveryMethodRepository extends JpaRepository<DeliveryMethod, Long> {
}
