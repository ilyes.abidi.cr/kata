package com.kata.driveanddeliver.repository;

import com.kata.driveanddeliver.model.DeliveryMethod;
import com.kata.driveanddeliver.model.TimeSlot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TimeSlotRepository extends JpaRepository<TimeSlot, Long> {

    /**
     * Recherche les créneaux horaires spécifiques à une méthode de livraison donnée.
     *
     * @param method la méthode de livraison
     * @return la liste des créneaux horaires associés à la méthode de livraison
     */
    List<TimeSlot> findByDeliveryMethod(DeliveryMethod method);

    List<TimeSlot> findByIsBookedFalse();
}
