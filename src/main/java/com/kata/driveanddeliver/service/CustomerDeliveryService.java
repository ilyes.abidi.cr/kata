package com.kata.driveanddeliver.service;
import com.kata.driveanddeliver.model.CustomerDelivery;
import com.kata.driveanddeliver.model.TimeSlot;
import com.kata.driveanddeliver.repository.CustomerDeliveryRepository;
import com.kata.driveanddeliver.repository.TimeSlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

@Service
public class CustomerDeliveryService {

    @Autowired
    private CustomerDeliveryRepository customerDeliveryRepository;

    @Autowired
    private TimeSlotRepository timeSlotRepository;

    @CacheEvict(value = {"availableTimeSlots"}, allEntries = true)
    public CustomerDelivery bookTimeSlot(CustomerDelivery customerDelivery) {
        TimeSlot timeSlot = timeSlotRepository.findById(customerDelivery.getTimeSlot().getId())
                .orElseThrow(() -> new IllegalArgumentException("Time slot not found"));

        if (Boolean.TRUE.equals(timeSlot.getIsBooked())) {
            throw new IllegalArgumentException("Time slot is already booked");
        }

        customerDelivery.getTimeSlot().setIsBooked(true);
        timeSlotRepository.save(customerDelivery.getTimeSlot());
        return customerDeliveryRepository.save(customerDelivery);
    }
}
