package com.kata.driveanddeliver.service;

import com.kata.driveanddeliver.model.DeliveryMethod;
import com.kata.driveanddeliver.repository.DeliveryMethodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeliveryMethodService {

    @Autowired
    private DeliveryMethodRepository deliveryMethodRepository;

    public List<DeliveryMethod> getAllDeliveryMethods() {
        return deliveryMethodRepository.findAll();
    }

    public DeliveryMethod getDeliveryMethodById(Long id) {
        return deliveryMethodRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Delivery method not found"));
    }
}
