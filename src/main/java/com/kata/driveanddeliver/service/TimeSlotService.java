package com.kata.driveanddeliver.service;

import com.kata.driveanddeliver.model.DeliveryMethod;
import com.kata.driveanddeliver.model.TimeSlot;
import com.kata.driveanddeliver.repository.TimeSlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TimeSlotService {

    @Autowired
    private TimeSlotRepository timeSlotRepository;

    /**
     * Récupère tous les créneaux horaires disponibles.
     *
     * @return la liste des créneaux horaires
     */
    @Cacheable("availableTimeSlots")
    public List<TimeSlot> getAllAvailableTimeSlots() {
        return timeSlotRepository.findByIsBookedFalse();
    }

    /**
     * Récupère tous les créneaux horaires.
     *
     * @return la liste des créneaux horaires
     */
    @Cacheable("allTimeSlots")
    public List<TimeSlot> getAllTimeSlots() {
        return timeSlotRepository.findAll();
    }

    /**
     * Récupère un créneau horaire par son identifiant.
     *
     * @param id l'identifiant du créneau horaire
     * @return le créneau horaire, ou null s'il n'existe pas
     */
    @Cacheable(value = "timeSlotById", key = "#id")
    public TimeSlot getTimeSlotById(Long id) {
        return timeSlotRepository.findById(id).orElse(null);
    }

    /**
     * Récupère les créneaux horaires spécifiques à une méthode de livraison donnée.
     *
     * @param method la méthode de livraison
     * @return la liste des créneaux horaires associés à la méthode de livraison
     */
    @Cacheable(value = "timeSlotsByMethod", key = "#method")
    public List<TimeSlot> getTimeSlotsByMethod(DeliveryMethod method) {
        return timeSlotRepository.findByDeliveryMethod(method);
    }
}
