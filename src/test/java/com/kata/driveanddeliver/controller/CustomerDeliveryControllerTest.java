package com.kata.driveanddeliver.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kata.driveanddeliver.model.CustomerDelivery;
import com.kata.driveanddeliver.service.CustomerDeliveryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.BDDMockito.given;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;

@WebMvcTest(CustomerDeliveryController.class)
@ActiveProfiles("test")
class CustomerDeliveryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerDeliveryService customerDeliveryService;

    @Test
    void testBookDeliveryUnauthorized() throws Exception {
        CustomerDelivery customerDelivery = new CustomerDelivery();
        CustomerDelivery bookedDelivery = new CustomerDelivery();

        given(customerDeliveryService.bookTimeSlot(customerDelivery)).willReturn(bookedDelivery);

        String customerDeliveryJson = convertToJson(customerDelivery);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/book-delivery")
                        .with(SecurityMockMvcRequestPostProcessors.csrf()) // Correctly adding the CSRF token
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(customerDeliveryJson))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    @WithMockUser(username="user")
    void testBookDelivery() throws Exception {
        CustomerDelivery customerDelivery = new CustomerDelivery();
        CustomerDelivery bookedDelivery = new CustomerDelivery();

        given(customerDeliveryService.bookTimeSlot(customerDelivery)).willReturn(bookedDelivery);

        String customerDeliveryJson = convertToJson(customerDelivery);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/book-delivery")
                        .with(SecurityMockMvcRequestPostProcessors.csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(customerDeliveryJson))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    private String convertToJson(Object obj) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}