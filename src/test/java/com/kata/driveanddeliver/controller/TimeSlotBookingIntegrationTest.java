package com.kata.driveanddeliver.controller;

import com.kata.driveanddeliver.model.Customer;
import com.kata.driveanddeliver.model.CustomerDelivery;
import com.kata.driveanddeliver.model.DeliveryMethod;
import com.kata.driveanddeliver.model.TimeSlot;
import com.kata.driveanddeliver.repository.CustomerRepository;
import com.kata.driveanddeliver.repository.DeliveryMethodRepository;
import com.kata.driveanddeliver.repository.TimeSlotRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class TimeSlotBookingIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TimeSlotRepository timeSlotRepository;

    @Autowired
    private DeliveryMethodRepository deliveryMethodRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @BeforeEach
    void setup() {
        // Setup data for DeliveryMethod
        DeliveryMethod method = new DeliveryMethod();
        method.setName("Standard Delivery");
        deliveryMethodRepository.save(method);

        // Setup data for Customer
        Customer customer = new Customer();
        customer.setName("Customer");
        customerRepository.save(customer);

        // Setup data for TimeSlot
        TimeSlot slot1 = new TimeSlot();
        slot1.setDeliveryMethod(method);
        slot1.setDay(LocalDate.now());
        slot1.setStartTime(LocalTime.of(9, 0));
        slot1.setEndTime(LocalTime.of(12, 0));
        slot1.setIsBooked(false);
        timeSlotRepository.save(slot1);

        TimeSlot slot2 = new TimeSlot();
        slot2.setDeliveryMethod(method);
        slot2.setDay(LocalDate.now());
        slot2.setStartTime(LocalTime.of(13, 0));
        slot2.setEndTime(LocalTime.of(16, 0));
        slot2.setIsBooked(true);
        timeSlotRepository.save(slot2);
    }

    @Test
    void testGetAndBookTimeSlot() {
        // Initially make a harmless request to get the CSRF token
        ResponseEntity<String> csrfTokenResponse = restTemplate.getForEntity("http://localhost:" + port + "/public/time-slots/available", String.class);
        String csrfToken = extractCsrfToken(csrfTokenResponse);

        // Retrieve all available time slots
        ResponseEntity<List> response = restTemplate.getForEntity("http://localhost:" + port + "/public/time-slots/available", List.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotEmpty();
        assertThat(response.getBody()).hasSize(1);

        //for testing retrieve the customer
        Customer customer = customerRepository.findById(1L).orElseThrow();

        List<Map<String, Object>> slots = response.getBody();
        if (!slots.isEmpty()) {
            Map<String, Object> firstSlotMap = slots.get(0);
            TimeSlot firstSlot = mapToTimeSlot(firstSlotMap);
            CustomerDelivery customerDelivery = new CustomerDelivery();
            customerDelivery.setTimeSlot(firstSlot);
            customerDelivery.setCustomer(customer);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("X-CSRF-TOKEN", csrfToken); // Add CSRF token to the headers
            HttpEntity<CustomerDelivery> request = new HttpEntity<>(customerDelivery, headers);

            ResponseEntity<CustomerDelivery> bookResponse = restTemplate.postForEntity("http://localhost:" + port + "/api/book-delivery", request, CustomerDelivery.class);
            assertThat(bookResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
            assertThat(Objects.requireNonNull(bookResponse.getBody()).getTimeSlot().getIsBooked()).isTrue();
        } else {
            fail("No available slots to book.");
        }
    }

    private String extractCsrfToken(ResponseEntity<String> response) {
        // Typically, CSRF tokens might be stored in a cookie or header
        List<String> setCookie = response.getHeaders().get(HttpHeaders.SET_COOKIE);
        if (setCookie != null) {
            return setCookie.stream()
                    .map(this::parseCsrfTokenFromCookie)
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElse(null);
        }
        return null;
    }

    private String parseCsrfTokenFromCookie(String cookie) {
        Pattern pattern = Pattern.compile("XSRF-TOKEN=(.+?);");
        Matcher matcher = pattern.matcher(cookie);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    private TimeSlot mapToTimeSlot(Map<String, Object> slotMap) {
        TimeSlot slot = new TimeSlot();
        slot.setId(Long.valueOf(slotMap.get("id").toString()));
        slot.setDay(LocalDate.parse(slotMap.get("day").toString()));
        slot.setStartTime(LocalTime.parse(slotMap.get("startTime").toString()));
        slot.setEndTime(LocalTime.parse(slotMap.get("endTime").toString()));
        slot.setIsBooked((Boolean) slotMap.get("isBooked"));
        return slot;
    }

}