package com.kata.driveanddeliver.repository;

import com.kata.driveanddeliver.model.DeliveryMethod;
import com.kata.driveanddeliver.model.TimeSlot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class TimeSlotRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TimeSlotRepository timeSlotRepository;

    @BeforeEach
    void setup() {
        // Setup data for DeliveryMethod
        DeliveryMethod method1 = new DeliveryMethod();
        method1.setName("Standard Delivery");
        entityManager.persist(method1);

        // Setup data for TimeSlot
        TimeSlot slot1 = new TimeSlot();
        slot1.setDeliveryMethod(method1);
        slot1.setDay(LocalDate.now());
        slot1.setStartTime(LocalTime.of(9, 0));
        slot1.setEndTime(LocalTime.of(12, 0));
        slot1.setIsBooked(false);
        entityManager.persist(slot1);

        TimeSlot slot2 = new TimeSlot();
        slot2.setDeliveryMethod(method1);
        slot2.setDay(LocalDate.now().plusDays(1));
        slot2.setStartTime(LocalTime.of(13, 0));
        slot2.setEndTime(LocalTime.of(16, 0));
        slot2.setIsBooked(false);
        entityManager.persist(slot2);

        entityManager.flush();
    }

    @Test
    void testFindByDeliveryMethod() {
        DeliveryMethod method = entityManager.find(DeliveryMethod.class, 1L);
        List<TimeSlot> slots = timeSlotRepository.findByDeliveryMethod(method);
        assertThat(slots).hasSize(2);
        assertThat(slots.get(0).getDay()).isEqualTo(LocalDate.now());
    }
}
